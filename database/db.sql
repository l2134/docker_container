CREATE TABLE  movie (movie_id int PRIMARY KEY auto_increment, movie_title VARCHAR(30),
movie_release_date date,
 movie_time float,
 director_name VARCHAR(30));