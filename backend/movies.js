const express = require('express')
const db = require('./db')
const utils = require('./utils')

const router = express.Router()

//add new movie
router.post('/', (request, response) => {
    const { movie_title,movie_release_date,movie_time,
        director_name } = request.body
    const query = `
      INSERT INTO movie
        (movie_title,movie_release_date,movie_time,
            director_name)
      VALUES
        ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })



  // getting all
  router.get('/', (request, response) => {
    const query = `
      SELECT *
      FROM movie
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })

  //update movie

  router.put('/:movie_id', (request, response) => {
    const { id } = request.params
    const { movie_title,movie_release_date,movie_time,
        director_name  } = request.body
  
    const query = `
      UPDATE movie
      SET
      movie_title = '${movie_title}', 
      movie_release_date = '${movie_release_date}',
      movie_time = '${movie_time}',
      director_name = '${director_name}'
      WHERE
      movie_id = ${id}
    `
    db.execute(query, (error, result) => {
      response.send(utils.createResult(error, result))
    })
  })
  
    //delete movie
router.delete('/:movie_id', (request, response) => {
const { id } = request.params

const query = `
  DELETE FROM movie
  WHERE
  movie_id = ${id}
`
db.execute(query, (error, result) => {
  response.send(utils.createResult(error, result))
})
})

module.exports = router

